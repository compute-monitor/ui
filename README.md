## 计算机资源监控UI
此为UI界面，内容很简单，就是访问接口获取数据，稍微处理一下，然后显示出来

### 使用方法

如果是在有node.js的环境下使用，直接“npm run dev”

如果是编译部署到服务器上，需要依赖nginx，nginx的部署与使用网上有资料，这里不再重复
#### 服务器部署
1. 运行指令“npm build”,构建项目，就会在当前目录下生成一个“dist”目录，里面的文件就是构建好的目录
2. 将目录复制到服务器的某个目录下
3. 然后再nginx的配置文件上配置：
    ```
        server {
            listen       10000;
            server_name  localhost;

            location / {
                root   E:\service\monitorUI;
                index  index.html;
            try_files $uri /index.html;
            }

            location /html {
            add_header Cache-Control no-store;
            }
        }
    ```
    配置文件中的路径需要自己修改，”dist”我改名为“monitorUI”了，下面这个location是为了处理项目更新后浏览器刷新却不生效的问题。
4. 重新加载配置"nginx -s reload"之后就可以使用ip:port访问了。

### 框架介绍

框架采用Vue3+Vite+TypeScript+element Plus，可以看：[这篇分析](https://blog.csdn.net/lsjweiyi/article/details/123236114?spm=1001.2014.3001.5501)

也是当前比较流行的技术

### 状态管理
我采用pinia做状态管理，也可以理解为全局变量，方便页面数据的更新，系列文章里有分析，这里就不再介绍。


### 数据分析

代码位于“src/service”下。首先有一个接收数据的结构体“models/source.ts”.

然后“api/source.ts”获取数据和处理数据，详情看代码，注释很详细

### 页面卡片显示

页面也很简单，采用el-card实现，相关资料可以看[element plus官网](https://element-plus.gitee.io/zh-CN/component/card.html)

代码在“src/components/Card.vue“ ，它就调用数据接口更新数据，然后将数据按照形式展现出来，可以看代码解析。


