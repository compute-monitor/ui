import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { axiosConfig } from './utils/http'


import '~/styles/index.scss'

// If you want to use ElMessage, import it.
import 'element-plus/theme-chalk/src/message.scss'

const app = createApp(App)

axiosConfig() // 加载axios配置
// app.use(ElementPlus);
app.use(createPinia()).use(router).use(VueAxios, axios).mount('#app')
