
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import HelloWorld from '@/Components/HelloWorld.vue'
// 路由列表，路由就添加在此 （暂时没用上）
const routes: Array<RouteRecordRaw> = [
  // {
  //   path: '/HelloWorld',
  //   name: 'HelloWorld',
  //   component: HelloWorld
  // }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
