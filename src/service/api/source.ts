
import { get } from '@/utils/http/request'
import { useSourceStore } from "~/store/index";
import response from '../models/source'

// 接口
function getHealth<T>() {
    return get<T>('v1/percent')
}


// 访问接口获取数据并处理数据
export function dealSource() {
    getHealth<response>().then((res) => {
        const myStore = useSourceStore(); // 全局变量对象，将数据赋值到此，页面通过该全局变量获取数据
        const responseMsg: response = res.data
        const deleIndex: number[] = []  // 记录超过一小小时未更新数据的机器的下标
        responseMsg.data.forEach((element, index) => {

            const getSourceTime = new Date(element.time).getTime() // 转换时间格式
            if (new Date().getTime() - getSourceTime > 60000) { // 与当前时间超过一分钟

                element.timeout = element.time  // 如果有内容。会在标题右侧显示，可按需定制内容
            }
            element.source = JSON.parse(String(element.source)) // 将json字符串转化为json对象
        });

        myStore.$state.source = responseMsg.data // 赋值给全局变量
    });
}