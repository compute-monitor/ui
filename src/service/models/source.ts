interface source {
    cpuPer: number,
    memPer: number,
    diskPerList: {
        C: number,
        D: number,
        E: number,
        F: number,
        G: number
    }
}

interface response {
    code: number,
    data: [
        {
            id: number,
            tableName: string,
            time: string,
            timeout: string, // 时间超出正常范围的提示语
            source: source
        }
    ]
}

export default response;