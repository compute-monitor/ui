import { defineStore } from 'pinia'
import response from '~/service/models/source'
//全局变量对象

// 由于typescript强类型语言，这里就是实现一个对象，方便后续识别调用数据
const responseMsg: response = {
  code: 0,

  data: [
    {
      id: 0,
      tableName: "",
      time: "",
      timeout: "", // 时间超出正常范围的提示语
      source: {
        cpuPer: 0,
        memPer: 0,
        diskPerList: {
          C: 0,
          D: 0,
          E: 0,
          F: 0,
          G: 0
        }
      }
    }
  ]
}
export const useSourceStore = defineStore({
  id: 'source',
  state: () => ({
    source: responseMsg.data // 全局变量
  })
})

