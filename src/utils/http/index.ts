import axios from "axios";
import { AxiosRequestConfig, AxiosResponse } from "axios";

// axios配置在此实现
export const axiosConfig = function () {
  axios.defaults.baseURL = "http://localhost:10001/"; // 设置服务端地址
  axios.defaults.timeout = 3000; // 设置超时时间
  axios.interceptors.request.use( // 请求中间件
    (config: AxiosRequestConfig) => {
      /**
       * 实现自己的业务逻辑
       * 1.开启全屏加载动画之类
       * 2.数据加密config。data
       * 3.给请求头添加信息等（token 结合sessionStorage，localStorage,vuex这些）
       *
       */
      return config;
    },
    (error) => {
      /* 请求错误的业务逻辑
           1. 关闭全屏loading动画
           2. 重定向到错误页
         */
      return Promise.reject(error); // 为了可以在代码中catch到错误信息
    }
  ); //end request.use

  axios.interceptors.response.use( // 响应中间件
    (response: AxiosResponse) => {
      /* 
        1. 关闭全屏loading动画
        2. 数据解密
        3. 根据 response.data.code 做不同的错误处理
        4. ……
        */
      return response;
    },
    (error) => {
      return Promise.reject(error);
    }
  ); //end response.use
};
