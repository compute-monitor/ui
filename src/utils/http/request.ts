import axios from "axios";
import { IResponseData } from "./type";

// 实现gey方法
// 这里采用泛型T实现，能够达到数据直接解析成对应的对象
export function get<T>(url: string, params?: any): Promise<IResponseData<T>> {
  return axios.get(url, params);
}

// 实现post方法
export function post<T>(url: string, params: any): Promise<IResponseData<T>> {
  return axios.post(url, params);
}
