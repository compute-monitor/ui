// 接收response的对象类
export interface IResponseData<T> {
  status: number;
  message?: string;
  data: T;
  code: string;
}
